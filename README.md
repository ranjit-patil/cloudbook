# CloudBook

## Introduction
This repository contains a PHP script that allows you to connect your Zoho Accounting data with Power BI for analysis and visualization. By using this connector, you can seamlessly integrate your financial data from Zoho Accounting into Power BI reports and dashboards.

## Features
- **Easy Integration**: Quickly connect your Zoho Accounting account with Power BI using this PHP script.
- **Secure Authentication**: Ensure secure authentication between Zoho Accounting and Power BI.
- **Real-time Data Sync**: Fetch real-time data from Zoho Accounting to Power BI for up-to-date analysis.
- **Customizable Queries**: Customize queries to retrieve specific data sets based on your business needs.
- **User-Friendly**: Simple and intuitive interface for easy setup and usage.

## Requirements
- PHP server environment
- Zoho Accounting API credentials
- Power BI account

## Installation
1. Clone or download the repository to your local machine.
2. Copy the `config.sample.php` file and rename it to `config.php`.
3. Open the `config.php` file and provide your Zoho Accounting API credentials.
4. Upload the PHP script to your web server or hosting environment.

## Usage
1. Configure the PHP script with your Zoho Accounting API credentials.
2. Customize the queries in the script to retrieve the desired data from Zoho Accounting.
3. Import the data into Power BI using the PHP script as a data source.
4. Create reports and dashboards in Power BI using the imported data from Zoho Accounting.

## Support
If you encounter any issues or have any questions about using the Zoho Accounting to Power BI Connector, please Skype us with id 8308775709.

## License
This project is licensed under the [Apache License 2.0](https://gitlab.com/ranjit-patil/cloudbook/license).

## Contributors
- [Ranjeet G](https://gitlab.com/ranjit-patil) - Project Lead
- [Ravina Kale](https://gitlab.com/RavinaKale)
